import java.io.*;
import java.util.*;
import java.lang.*;

class aoc {
  public static void main(String[] args) {
    List<String> Ids = new ArrayList<>();
    Scanner reader;
    int index = 0;

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        Ids.add(reader.nextLine());
      }
      for (int i = 0; i < Ids.size(); i++) {
        String[] iString = Ids.get(i).split("");
        for(int j = 0; j < Ids.size(); j++){
          if (j != i) {
            String[] jString = Ids.get(j).split("");
            int difference = 0;
            for (int k = 0; k < jString.length; k++) {
              if (jString[k].equals(iString[k]) == false) {
                difference++;
                index = k;
              }
            }
            if (difference == 1) {
              for (int k = 0; k < jString.length; k++) {
                if (k != index){
                  System.out.print(jString[k]);
                }
              }
              System.out.println("");
            }
          }
        }
      }

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
