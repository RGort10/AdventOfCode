import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    List<String> Ids = new ArrayList<>();
    String alphabetString = "abcdefghijklmnopqrstuvwxyz";
    String[] alphabet = alphabetString.split("");
    int result;
    int countTwo = 0;
    int countThree = 0;
    Scanner reader;
    Boolean stop = false;

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        Boolean two = false;
        Boolean three = false;
        String[] idpart = reader.nextLine().split("");

        for (int i = 0; i < idpart.length; i++) {
          Ids.add(idpart[i]);
        }

        for (int i = 0; i < alphabet.length; i++) {
          int occurrences = Collections.frequency(Ids, alphabet[i]);
          if (occurrences == 3) {
            three = true;
          } else if(occurrences == 2){
              two = true;
          }
        }

        if(two){
          countTwo++;
        }
        if (three) {
          countThree++;
        }
        Ids.clear();
      }
      System.out.println("The checksum is: " + (countTwo * countThree));

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
