#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    int sum, inputNumber, i;

    fp = fopen("../input.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);


    fscanf (fp, "%d", &i);
    while (!feof (fp)){

      sum += i;
      printf("%d\n", sum);
      fscanf (fp, "%d", &i);
    }

    printf("The sum is: %d\n", sum);

    fclose(fp);
    if (line)
        free(line);
    exit(EXIT_SUCCESS);
    return 0;
}
