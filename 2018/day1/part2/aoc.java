import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    List<Integer> sum = new ArrayList<>();
    int result = 0;
    Scanner reader;
    Boolean stop = false;

    System.out.println("This operation will take a long time please be patient.");
    while(stop == false){
      try {
        reader = new Scanner(new File("../input.txt"));
        while (reader.hasNext()) {
          result += Integer.parseInt(reader.nextLine());

          if (sum.contains(result)) {
            System.out.println(result);
            stop = true;
            break;
          }

          sum.add(result);
        }
        reader.close();
        reader = null;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
