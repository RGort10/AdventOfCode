import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;
    List<String> list   = new ArrayList<String>();
    List<String> guards = new ArrayList<String>();
    List<Integer> sleep  = new ArrayList<Integer>();
    List<String> guardLines  = new ArrayList<String>();

    int begin = 0;
    int eind  = 0;
    try {
      System.out.println("This may take a while");
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        list.add(reader.nextLine());

      }
      Collections.sort(list);

      for (int i = 0; i < list.size(); i++) {
          String[] linePart = list.get(i).split(" ");
          if (linePart[2].equals("Guard")) {
            String guardID = linePart[3].split("#")[1];
            Boolean exitLoop = false;
            String[] hours = new String[60];
            Arrays.fill(hours, ".");
            do {
              if (list.size() == i+1) {
                exitLoop = true;
              } else {
                String[] nextLine = list.get(i+1).split(" ");
                if (nextLine[2].equals("Guard")) {
                  exitLoop = true;
                } else {
                  i++;

                  if(nextLine[2].equals("falls")) {
                    String tijd = nextLine[1].split("]")[0];
                    begin = Integer.parseInt(tijd.split(":")[1]);
                  } else if(nextLine[2].equals("wakes")) {
                    String tijd = nextLine[1].split("]")[0];
                    eind = Integer.parseInt(tijd.split(":")[1]);
                    for (int j = begin; j < eind; j++) {
                      hours[j] = "#";
                    }
                  }
                }
              }

            } while (exitLoop == false);

            int n = 0;
            String sleepMinutes = "";
            for (int m = 0; m < hours.length; m++) {
              sleepMinutes += hours[m];
              if (hours[m].equals("#")) {
                n++;
              }
            }

            if (guards.contains(guardID)) {
              int sleepHours = sleep.get(guards.indexOf(guardID));
              sleepHours += n;
              sleep.set(guards.indexOf(guardID), sleepHours);
            } else {
              guards.add(guardID);
              sleep.add(n);
            }
            String sleepLine = "Guard " + guardID + "   " + sleepMinutes + "\n";
            if (guards.size() == guardLines.size()) {
              String sleepLines = guardLines.get(guards.indexOf(guardID));
              sleepLines += sleepLine;
              guardLines.set(guards.indexOf(guardID), sleepLines);
            } else {
              guardLines.add(sleepLine);
            }

          }
      }


      List<Integer> biggestNumberIndex = new ArrayList<Integer>();
      biggestNumberIndex.add(0);
      int biggestNumber = sleep.get(0);

      for (int i = 1; i < guards.size(); i++) {
        if (sleep.get(i) > biggestNumber) {
          biggestNumber = sleep.get(i);
          biggestNumberIndex.clear();
          biggestNumberIndex.add(i);
        } else if (sleep.get(i) == biggestNumber) {
          biggestNumberIndex.add(i);
        }
      }

      System.out.println(guards.get(biggestNumberIndex.get(0)));
      System.out.println("            0000000000111111111122222222223333333333444444444445555555555");
      System.out.println("Guard 401   0123456789012345678901234567890123456789012345678901234567890");
      System.out.println(guardLines.get(biggestNumberIndex.get(0)));
      //System.out.println(sleep.get(biggestNumberIndex.get(0)) * Integer.parseInt(guardLines.get(biggestNumberIndex.get(0))));

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
