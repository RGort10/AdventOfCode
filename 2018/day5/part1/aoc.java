import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;

    List<String> inputChars = new ArrayList<>();


    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String[] chars = reader.nextLine().split("");

        for (int i = 0; i<chars.length; i++) {
          inputChars.add(chars[i]);
        }

        for (int i = 0; i < inputChars.size(); i++) {
          if (i != inputChars.size() - 1) {
            if (inputChars.get(i).toLowerCase().equals(inputChars.get(i+1).toLowerCase())) {
              if (!inputChars.get(i).equals(inputChars.get(i+1))) {
                inputChars.remove(i);
                inputChars.remove(i);
                i = 0;
              }
            }
          }
        }



      }
      System.out.println(inputChars.size());

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
