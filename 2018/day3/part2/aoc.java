import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;
    List<Boolean> claims = new ArrayList<Boolean>();
    String[][] fabric = new String[2000][2000];
    try {
      System.out.println("This may take a while");
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        Boolean overlap = false;
        String[] claimPart = reader.nextLine().split(" ");
        String id = claimPart[0].split("#")[1];
        String[] xyPositionPart = claimPart[2].split(":");
        String[] xyPosition = xyPositionPart[0].split(",");
        int xy[] = new int[2];
        xy[0] = Integer.parseInt(xyPosition[0]);
        xy[1] = Integer.parseInt(xyPosition[1]);

        String[] sizePosition = claimPart[3].split("x");
        int size[] = new int[2];
        size[0] = Integer.parseInt(sizePosition[0]);
        size[1] = Integer.parseInt(sizePosition[1]);


        int xn = xy[0] + size[0];
        int yn = xy[1] + size[1];
        for (int x = xy[0]; x < xn; x++) {
          for (int y = xy[1]; y < yn; y++) {
            if (fabric[x][y] == null) {
              fabric[x][y] = id;
            } else if (fabric[x][y] == "X") {
              overlap = true;
            } else {
              claims.set(Integer.parseInt(fabric[x][y]) -1 , true);
              fabric[x][y] = "X";
              overlap=true;
            }
          }
        }

        claims.add(overlap);
      }
      System.out.println("The claim that doens't overlap is #" + (claims.indexOf(false) + 1));
      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
