import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;
    String[][] fabric = new String[2000][2000];
    int overlaps = 0;
    try {
      System.out.println("This may take a while");
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String[] claimPart = reader.nextLine().split(" ");

        String[] xyPositionPart = claimPart[2].split(":");
        String[] xyPosition = xyPositionPart[0].split(",");
        int xy[] = new int[2];
        xy[0] = Integer.parseInt(xyPosition[0]);
        xy[1] = Integer.parseInt(xyPosition[1]);

        String[] sizePosition = claimPart[3].split("x");
        int size[] = new int[2];
        size[0] = Integer.parseInt(sizePosition[0]);
        size[1] = Integer.parseInt(sizePosition[1]);

        int xn = xy[0] + size[0];
        int yn = xy[1] + size[1];
        for (int x = xy[0]; x < xn; x++) {
          for (int y = xy[1]; y < yn; y++) {
            if (fabric[x][y] == null) {
              fabric[x][y] = "A";
            } else if (fabric[x][y] == "X") {

            } else {
              fabric[x][y] = "X";
              overlaps++;
            }
          }
        }

      }
      System.out.println(overlaps);
      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
