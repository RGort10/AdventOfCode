#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int main(void)
{
  /* Declaration for fileReader */
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  /* End declaration */

  char (*elements)[10] = malloc(8000);

  fp = fopen("../input.txt", "r");
  if (fp == NULL)
      exit(EXIT_FAILURE);

  int i = 0;
  while ((read = getline(&line, &len, fp)) != -1) {
    char *ptr = strtok(line, ",");
  	while(ptr != NULL)
  	{
      strcpy(elements[i], ptr);
      i++;
  		ptr = strtok(NULL, ",");
  	}
  }

  strcpy(elements[1], "12");
  strcpy(elements[2], "2");

  int elementsInArray = i - 1;
  i = 0;
  while (i < elementsInArray) {
    if (strcmp("1", elements[i]) == 0) {
      i++;
      int a = atoi(elements[atoi(elements[i])]);
      i++;
      int b = atoi(elements[atoi(elements[i])]);
      i++;
      int sum = a + b;
      sprintf(elements[atoi(elements[i])], "%d", sum);
      i ++;
    } else if (strcmp("2", elements[i]) == 0) {
      i++;
      int a = atoi(elements[atoi(elements[i])]);
      i++;
      int b = atoi(elements[atoi(elements[i])]);
      i++;
      int sum = a * b;
      sprintf(elements[atoi(elements[i])], "%d", sum);
      i ++;
    } else if (strcmp("99", elements[i]) == 0) {
      printf("The value left over at position 0 is: %s\n", elements[0]);
      i += 1000;
    } else {
      printf("ERROR %d ERROR element %s != 1 or 2 or 99\n", i, elements[i]);
      return 0;
    }

  }

  free(elements);
  free(line);
  exit(EXIT_SUCCESS);

  return 0;
}
