#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>

void printArray(char *arr, int size)
{
    int i;

    for (i = 0; i < size; i++)
    {
        printf("%d, ", *arr);
    }
}

int main(void)
{
  /* Declaration for fileReader */
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  /* End declaration */

  bool exitLoop = true;
  int noun = 0, verb = 0;
  char instructions[5000];

  fp = fopen("../input.txt", "r");
  if (fp == NULL)
      exit(EXIT_FAILURE);

  int i = 0;
  while ((read = getline(&line, &len, fp)) != -1) {
    strcpy(instructions, line);
  }


  while (exitLoop && noun < 100) {
    bool exitNestedLoop = true;
    verb = 0;
    while (exitNestedLoop && verb < 100){
      char (*intCodes)[10] = malloc(8000);
      char *ptr = strtok(ptr, ",");
      printArray(ptr, sizeof(ptr));
      printf("%s\n", ptr);
      while(ptr != NULL)
      {
        strcpy(intCodes[i], ptr);
        i++;
        ptr = strtok(NULL, ",");
      }

      int elementsInArray = i - 1;

      sprintf(intCodes[1], "%d", noun);
      sprintf(intCodes[2], "%d", verb);

      i = 0;
      while (i < elementsInArray) {
        if (noun == 10) {
        }
        if (strcmp("1", intCodes[i]) == 0) {
          i++;
          int a = atoi(intCodes[atoi(intCodes[i])]);
          i++;
          int b = atoi(intCodes[atoi(intCodes[i])]);
          i++;
          int sum = a + b;
          sprintf(intCodes[atoi(intCodes[i])], "%d", sum);
          i ++;
        } else if (strcmp("2", intCodes[i]) == 0) {
          i++;
          int a = atoi(intCodes[atoi(intCodes[i])]);
          i++;
          int b = atoi(intCodes[atoi(intCodes[i])]);
          i++;
          int sum = a * b;
          sprintf(intCodes[atoi(intCodes[i])], "%d", sum);
          i ++;
        } else if (strcmp("99", intCodes[i]) == 0) {

          if (strcmp("19690720", intCodes[0]) == 0) {
            printf("%s\n", intCodes[0]);
            printf("100 * noun * verb = ");
            printf("100 * %d * %d = %d\n", noun, verb, 100 * noun * verb);
            exitLoop = false;
            exitNestedLoop = false;
          }
          i += 1000;
        } else {
          printf("ERROR %d ERROR element %s != 1 or 2 or 99\n", i, intCodes[i]);
          return 0;
        }

      }
      free(line);
      printf("%d\n", verb);
      verb++;


    }
    noun++;
  }

  printf("%d\n",exitLoop);

    return 0;
}
