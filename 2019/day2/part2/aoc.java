import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;

    List<String> Ids = new ArrayList<>();

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String[] intCodeMaster = reader.nextLine().split(",");

        int a, b, sum;
        int noun = 0;

        while(noun < 100){
          int verb = 0;
          while(verb<100){
            String[] intCode = new String[intCodeMaster.length];
            for (int j = 0; j < intCode.length; j++) {
              intCode[j] = intCodeMaster[j];
            }
            intCode[1] = Integer.toString(noun);
            intCode[2] = Integer.toString(verb);

            int i = 0;
            while(i < intCode.length){
              switch (intCode[i]) {
                case "1":
                  i++;
                  a = Integer.parseInt(intCode[Integer.parseInt(intCode[i])]);
                  i++;
                  b = Integer.parseInt(intCode[Integer.parseInt(intCode[i])]);
                  sum = a + b;
                  i++;
                  intCode[Integer.parseInt(intCode[i])] = Integer.toString(sum);
                  break;
                case "2":
                  i++;
                  a = Integer.parseInt(intCode[Integer.parseInt(intCode[i])]);
                  i++;
                  b = Integer.parseInt(intCode[Integer.parseInt(intCode[i])]);
                  sum = a * b;
                  i++;
                  intCode[Integer.parseInt(intCode[i])] = Integer.toString(sum);
                  break;
                case "99":
                  if (intCode[0].equals("19690720")) {
                    int answer = 100 * noun + verb;
                    System.out.println("100 * noun + verb = 100 * " + noun + " + " + verb + " = " + answer);
                    System.exit(0);
                  }
                  i = intCode.length + 2;
                  break;
                default:
                  System.out.println("Error Not 1, 2, 99 ~~ " + noun + ", " + verb);
                  System.out.println(i + " ~~ " + intCode[i]);
                  System.exit(-1);
              }
              i++;
            }
            verb++;
          }
          noun++;
        }

      }

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
