import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    List<Integer> possiblePasswords = new ArrayList<>();

    for (int i = 134564; i < 585159; i++) {
      String[] numberPart = Integer.toString(i).split("");
      List<Integer> numberP = new ArrayList<>();

      boolean doubleNumber = false;
      boolean increase = true;

      for (int j = 0; j<numberPart.length; j++) {
        numberP.add(Integer.parseInt(numberPart[j]));
      }

      Collections.sort(numberP);

      for (int j = 0; j < 10; j++) {
        if (Collections.frequency(numberP, j) == 2)
          doubleNumber = true;
      }

      for (int j = 0; j<numberPart.length; j++) {
        if (!numberPart[j].equals(Integer.toString(numberP.get(j))))
          increase = false;
      }

      if (doubleNumber && increase) {
        possiblePasswords.add(i);
      }
    }

    System.out.println(possiblePasswords.size());

  }
}
