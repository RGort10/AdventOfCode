import java.io.*;
import java.util.*;

class aoc {

  public static Coordinates Coord(int x, int y, int size){
    Coordinates c = new Coordinates(x, y, size);
    return c;
  }

  public static void main(String[] args) {
    System.out.println("This can take a while");
    Scanner reader;

    List<Coordinates> lineCoordinatesA = new ArrayList<Coordinates>();
    List<Coordinates> lineCoordinatesB = new ArrayList<Coordinates>();
    List<Coordinates> intersectionCoordinates = new ArrayList<Coordinates>();

    try {
      reader = new Scanner(new File("../input.txt"));
      int countLines = 0;
      while (reader.hasNext()) {
        int x = 0;
        int y = 0;
        String[] instructions = reader.nextLine().split(",");
        int count = 0;
        final int totalCount = instructions.length;
        for (String instruct : instructions) {
          String[] direction = instruct.split("");
          String move = "";
          for (int i = 1; i < direction.length; i++) {
            move += direction[i];
          }
          int movement = Integer.parseInt(move);
          for (int i = 0; i < movement; i++) {
            count++;
            switch (direction[0]) {
              case "U":
                y++;
                break;

              case "D":
                y--;
                break;

              case "R":
                x++;
                break;

              case "L":
                x--;
                break;

              default:
                System.out.println("Error With direction");
            }
            if (countLines == 0) {
              lineCoordinatesA.add(Coord(x, y, count));
            } else {
              lineCoordinatesB.add(Coord(x, y, count));
            }
          }
        }
        countLines++;
      }

      for (Coordinates coord : lineCoordinatesA) {
        int indexOfB = lineCoordinatesB.indexOf(coord);
        if (indexOfB != -1) {
          int combinedSteps = lineCoordinatesB.get(indexOfB).steps + coord.steps;
          intersectionCoordinates.add(Coord(coord.x, coord.y, combinedSteps));
        }
      }

      int closestManhattan = 1000000;
      int fewestCombinedSteps = 10000000;

      for (Coordinates coord : intersectionCoordinates) {
        int manhattan = Math.abs(0-coord.x) + Math.abs(0-coord.y);
        if (manhattan < closestManhattan)
          closestManhattan = manhattan;

        if (coord.steps < fewestCombinedSteps)
          fewestCombinedSteps = coord.steps;

      }

      System.out.println("closest manhattan distanse: " + closestManhattan);
      System.out.println("fewest combined steps: " + fewestCombinedSteps);


      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
