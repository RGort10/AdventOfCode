public class Coordinates{
  public int x;
  public int y;

  public Coordinates(int x, int y){
    this.x = x;
    this.y = y;
  }

  public boolean equals(Object coordObj) {
        boolean retVal = false;

        if (coordObj instanceof Coordinates){
            Coordinates ptr = (Coordinates) coordObj;
            retVal = ptr.x == this.x && ptr.y == this.y ;
        }

     return retVal;
  }

}
