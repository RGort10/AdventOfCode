import java.io.*;
import java.util.*;

class aoc {
  public static int StoI(String input){
    return Integer.parseInt(input);
  }

  public static String ItoS(int input){
    return Integer.toString(input);
  }

  public static void main(String[] args) {
    /*Scanners*/
    Scanner reader;
    Scanner scan = new Scanner(System.in);
    /*End Scanners*/

    System.out.println("This operation will take a long time please be patient.");

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        final String[] intCodes = reader.nextLine().split(",");
        int i = 0;
        while(i < intCodes.length) {
          final String[] instruction = intCodes[i].split("");
          String[] modes = {"0","0","0"};
          switch (instruction.length) {
            case 5:
              modes[0] = instruction[2];
              modes[1] = instruction[1];
              modes[2] = instruction[0];
              break;
            case 4:
              modes[0] = instruction[1];
              modes[1] = instruction[0];
              break;
            case 3:
              modes[0] = instruction[0];
              break;
          }

          int a = 0;
          int b = 0;
          int c = 0;
          int sum = 0;
          String value = "";

          switch (instruction[instruction.length - 1]) {
            case "1":
              i++;
              switch (modes[0]) {
                case "0":
                  a = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  a = StoI(intCodes[i]);
              }
              i++;
              switch (modes[1]) {
                case "0":
                  b = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  b = StoI(intCodes[i]);
              }
              i++;
              sum = a + b;
              intCodes[StoI(intCodes[i])] = ItoS(sum);
              i++;
              break;

            case "2":
              i++;
              switch (modes[0]) {
                case "0":
                  a = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  a = StoI(intCodes[i]);
              }
              i++;
              switch (modes[1]) {
                case "0":
                  b = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  b = StoI(intCodes[i]);
              }
              i++;
              sum = a * b;
              intCodes[StoI(intCodes[i])] = ItoS(sum);
              i++;
              break;

            case "3":
              i++;
              System.out.print("please input a number> ");
              String inputLine = scan.nextLine();
              System.out.print("\n");
              a = StoI(intCodes[i]);
              intCodes[a] = inputLine;
              i++;
              break;

            case "4":
              i++;
              a = StoI(intCodes[StoI(intCodes[i])]);
              System.out.println(a);
              i++;
              break;

            case "5":
              i++;
              switch (modes[0]) {
                case "0":
                  a = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  a = StoI(intCodes[i]);
              }
              i++;
              switch (modes[1]) {
                case "0":
                  b = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  b = StoI(intCodes[i]);
              }
              if (a != 0) {
                i = b;
              } else {
                i++;
              }
              break;

            case "6":
              i++;
              switch (modes[0]) {
                case "0":
                  a = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  a = StoI(intCodes[i]);
              }
              i++;
              switch (modes[1]) {
                case "0":
                  b = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  b = StoI(intCodes[i]);
              }
              if (a == 0) {
                i = b;
              } else {
                i++;
              }
              break;

            case "7":
              i++;
              switch (modes[0]) {
                case "0":
                  a = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  a = StoI(intCodes[i]);
              }
              i++;
              switch (modes[1]) {
                case "0":
                  b = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  b = StoI(intCodes[i]);
              }
              i++;
              if (a < b) {
                value = "1";
              } else {
                value = "0";
              }
              intCodes[StoI(intCodes[i])] = value;
              i++;
              break;

            case "8":
              i++;
              switch (modes[0]) {
                case "0":
                  a = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  a = StoI(intCodes[i]);
              }
              i++;
              switch (modes[1]) {
                case "0":
                  b = StoI(intCodes[StoI(intCodes[i])]);
                  break;
                case "1":
                  b = StoI(intCodes[i]);
              }
              i++;
              if (a == b) {
                value = "1";
              } else {
                value = "0";
              }
              intCodes[StoI(intCodes[i])] = value;
              i++;
              break;

            case "9":
              System.out.println("The value left over at position 0 is: " + intCodes[0]);
              i += intCodes.length;
              break;

            default:
              System.out.println("ERROR Intcode not found at intcode " + i + "  " + intCodes[i]);
              System.exit(-2);
          }
        }
      }
      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
