import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args)  throws InterruptedException{
    Scanner reader;

    try {
      List<String> layers = new ArrayList<>();

      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String[] lines = reader.nextLine().split("");
        int i = 0;
        while (i < lines.length) {
          String layer = "";
          for (int j = 0; j<6; j++) {
            for (int k = 0; k<25; k++) {
              layer += lines[i];
              i++;
            }
          }
          layers.add(layer);
        }
      }

      String[] picture = layers.get(layers.size() - 1).replace("2"," ").split("");

      for (int i = layers.size() - 1; i >= 0; i--) {
        String[] chars = layers.get(i).split("");
        for (int j = 0; j < chars.length; j++) {
          switch (chars[j]) {
            case "0":
              picture[j] = " ";
              break;

            case "1":
              picture[j] = "█";
              break;
          }
        }
        printImage(picture);
      }

      System.out.println("\n\n\n\n\n\n\n");
      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public static void printImage(String[] image) throws InterruptedException {
    for (int i = 0; i < 6; i++) {
      for (int j = i*25; j<i*25+25; j++) {
        System.out.print(image[j]);
      }
      System.out.println();
      Thread.sleep(1);

    }
    System.out.print("\033[6A");
  }
}
