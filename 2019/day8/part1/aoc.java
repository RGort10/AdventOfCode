import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;

    try {
      List<String> layers = new ArrayList<>();

      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String[] lines = reader.nextLine().split("");
        int i = 0;
        while (i < lines.length) {
          String layer = "";
          for (int j = 0; j<6; j++) {
            for (int k = 0; k<25; k++) {
              layer += lines[i];
              i++;
            }
          }
          layers.add(layer);
        }
      }
      int numberOfZeros = 9999999;
      int numberOfOnes = 0;
      int numberOfTwos = 0;

      for (int j = 0; j < layers.size(); j++) {
        int count = 0;
        int ones = 0;
        int twos = 0;
        String[] chars = layers.get(j).split("");
        for (String a : chars) {
          switch (a) {
            case "0":
              count++;
              break;

            case "1":
              ones++;
              break;

            case "2":
              twos++;
              break;
          }
        }

        if (count < numberOfZeros){
          numberOfZeros = count;
          numberOfOnes = ones;
          numberOfTwos = twos;
        }

      }

      System.out.println(numberOfOnes * numberOfTwos);

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}
