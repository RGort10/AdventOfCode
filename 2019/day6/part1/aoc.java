import java.io.*;
import java.util.*;

class aoc {
  static List<OrbitRelations> extraOrbits = new ArrayList<>();

  static List<OrbitRelations> instructions = new ArrayList<>();
  static List<Orbits> mapOfOrbits = new ArrayList<>();
  static int numberOfOrbits = 0;

  public static Orbits Orbit(String name, String nameParent, int indirectOrbits){
    Orbits orbit = new Orbits(name, nameParent, indirectOrbits);
    return orbit;
  }

  public static OrbitRelations OrbitRelation(String nameParent, String name){
    OrbitRelations orbitRelation = new OrbitRelations(nameParent, name);
    return orbitRelation;
  }

  public static void main(String[] args) {
    Scanner reader;


    mapOfOrbits.add(Orbit("COM", "", -1));

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String line = reader.nextLine();
        String[] linePart = line.split("\\)");
        extraOrbits.add(OrbitRelation(linePart[0], linePart[1]));
      }

      List<OrbitRelations> branches = new ArrayList<>();
      String parent = "COM";

      while (extraOrbits.size() > 0) {
        for (int i = 0; i < extraOrbits.size(); i++) {
          OrbitRelations oR = extraOrbits.get(i);
          if (mapOfOrbits.contains(Orbit(oR.nameParent, "", -1))) {
            int index = mapOfOrbits.indexOf(Orbit(oR.nameParent, "", 1));
            int indirect = mapOfOrbits.get(index).indirectOrbits + 1;
            mapOfOrbits.add(Orbit(oR.name, oR.nameParent, indirect));
            numberOfOrbits += indirect + 1;
            extraOrbits.remove(i);
            i--;
          }
        }
      }

      System.out.println( "\n" + numberOfOrbits);
      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
