public class Orbits {
  String name;
  String nameParent;
  int indirectOrbits;

  public Orbits(String name, String nameParent, int indirectOrbits){
    this.name = name;
    this.nameParent = nameParent;
    this.indirectOrbits = indirectOrbits;
  }

  public boolean equals(Object Obj) {
        boolean retVal = false;

        if (Obj instanceof Orbits){
            Orbits ptr = (Orbits) Obj;
            retVal = ptr.name.equals(this.name);
        }

     return retVal;
  }
}
