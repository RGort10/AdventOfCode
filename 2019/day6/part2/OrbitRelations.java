public class OrbitRelations {
  String nameParent;
  String name;

  public OrbitRelations(String nameParent, String name){
    this.name = name;
    this.nameParent = nameParent;
  }

  public boolean equals(Object Obj) {
        boolean retVal = false;

        if (Obj instanceof OrbitRelations){
            OrbitRelations ptr = (OrbitRelations) Obj;
            retVal = ptr.nameParent.equals(this.nameParent);
        }

     return retVal;
  }
}
