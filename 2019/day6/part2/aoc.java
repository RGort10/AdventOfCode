import java.io.*;
import java.util.*;

class aoc {


  public static Orbits Orbit(String name, String[] nameParent, int indirectOrbits){
    Orbits orbit = new Orbits(name, nameParent, indirectOrbits);
    return orbit;
  }

  public static OrbitRelations OrbitRelation(String nameParent, String name){
    OrbitRelations orbitRelation = new OrbitRelations(nameParent, name);
    return orbitRelation;
  }

  public static String[] stoSA(String input) {
    return new String[]{input};
  }

  public static String[] SAtoSA(String[] a, String[] b){
       int length = a.length + b.length;
       String[] result = new String[length];
       System.arraycopy(a, 0, result, 0, a.length);
       System.arraycopy(b, 0, result, a.length, b.length);
       return result;
   }

  public static void main(String[] args) {
    Scanner reader;

    List<OrbitRelations> extraOrbits = new ArrayList<>();

    List<OrbitRelations> instructions = new ArrayList<>();
    List<Orbits> mapOfOrbits = new ArrayList<>();
    int numberOfOrbits = 0;

    mapOfOrbits.add(Orbit("COM", stoSA(""), -1));

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        String line = reader.nextLine();
        String[] linePart = line.split("\\)");
        extraOrbits.add(OrbitRelation(linePart[0], linePart[1]));
      }

      List<OrbitRelations> branches = new ArrayList<>();
      String parent = "COM";

      while (extraOrbits.size() > 0) {
        for (int i = 0; i < extraOrbits.size(); i++) {
          OrbitRelations oR = extraOrbits.get(i);
          if (mapOfOrbits.contains(Orbit(oR.nameParent, stoSA(""), -1))) {
            int index = mapOfOrbits.indexOf(Orbit(oR.nameParent, stoSA(""), 1));
            int indirect = mapOfOrbits.get(index).indirectOrbits + 1;
            String[] parents = mapOfOrbits.get(index).nameParents;
            mapOfOrbits.add(Orbit(oR.name, SAtoSA(parents, stoSA(oR.nameParent)), indirect));
            numberOfOrbits += indirect + 1;
            extraOrbits.remove(i);
            i--;
          }
        }
      }

      int index = mapOfOrbits.indexOf(Orbit("YOU", stoSA(""), -1));
      String[] parentsYou = mapOfOrbits.get(index).nameParents;
      List<String> listYou = Arrays.asList(parentsYou);

      index = mapOfOrbits.indexOf(Orbit("SAN", stoSA(""), -1));
      String[] parentsSan = mapOfOrbits.get(index).nameParents;
      List<String> listSan = Arrays.asList(parentsSan);

      for (int i = listYou.size() - 1; i >= 0; i--) {
        String planet = listYou.get(i);
        if (listSan.contains(planet)) {
          int indexSan = listSan.indexOf(planet);
          int lowestNumberOfSteps = (listYou.size() - i -1) + (listSan.size() - indexSan -1);
          System.out.println(lowestNumberOfSteps);
          break;
        }
      }

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
