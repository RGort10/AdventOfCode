public class Orbits {
  String name;
  String[] nameParents;
  int indirectOrbits;

  public Orbits(String name, String[] nameParents, int indirectOrbits){
    this.name = name;
    this.nameParents = nameParents;
    this.indirectOrbits = indirectOrbits;
  }

  public boolean equals(Object Obj) {
        boolean retVal = false;

        if (Obj instanceof Orbits){
            Orbits ptr = (Orbits) Obj;
            retVal = ptr.name.equals(this.name);
        }

     return retVal;
  }
}
