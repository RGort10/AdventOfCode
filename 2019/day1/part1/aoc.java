import java.io.*;
import java.util.*;

class aoc {
  public static void main(String[] args) {
    Scanner reader;
    int sum = 0;

    try {
      reader = new Scanner(new File("../input.txt"));
      while (reader.hasNext()) {
        double mass = Double.parseDouble(reader.nextLine());
        int fuel = (int) Math.floor(mass / 3.0) - 2;
        sum += fuel;
      }

      System.out.println(sum);

      reader.close();
      reader = null;
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
