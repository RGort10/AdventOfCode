#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void)
{
  FILE * fp;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  int sum;

  sum = 0;

  fp = fopen("../input.txt", "r");
  if (fp == NULL)
      exit(EXIT_FAILURE);


  while ((read = getline(&line, &len, fp)) != -1) {
    int mass = atoi(line);
    int fuel = (int) floor((mass / 3) - 2);
    sum += fuel;
  }

  printf("The req is: %d\n", sum);

  free(line);
  exit(EXIT_SUCCESS);

  return 0;
}
